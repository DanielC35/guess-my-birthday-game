from random import randint

name=input("Hi, What is your name? ")

months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]

count = 0

count = int(input("Let me quess your birthdate! How many guesses should I get?"))

for guess_number in range(0, count):
    month_random = randint(0,11)
    year_random = randint(1924, 2004)

    answer = input("Guess " + str(guess_number + 1) + " : " + name + " were you born in " + months[month_random] + " / " + str(year_random) + " ? (yes/no)")

    if answer == "yes":
        print("I knew it")
    elif guess_number == count-1:
        print("I am bored, Go away")
    else:
        print("Drat, Lemme try again")